package com.primsnob.homework;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<Integer> list;

    Model() {
        list = new ArrayList<>(3);
        list.add(0);
        list.add(0);
        list.add(0);
    }

    public int getElementValueAtIndex(int index) {
        return list.get(index);
    }

    public void setElementValueAtIndex(int index, int value) {
        list.set(index, value);
    }
}
