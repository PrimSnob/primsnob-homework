package com.primsnob.homework;

public class Presenter extends BasePresenter<Model, MainView> {

    Presenter() {
        setModel(new Model());
    }

    private int calcNewModelValue(int modelElementIndex) {
        int currentValue = model.getElementValueAtIndex(modelElementIndex);
        return currentValue + 1;
    }

    public void button1Click() {
        int newModelValue = calcNewModelValue(0);
        model.setElementValueAtIndex(0, newModelValue);
        getView().setButton1Text(newModelValue);
    }

    public void button2Click() {
        int newModelValue = calcNewModelValue(1);
        model.setElementValueAtIndex(1, newModelValue);
        getView().setButton2Text(newModelValue);
    }

    public void button3Click() {
        int newModelValue = calcNewModelValue(2);
        model.setElementValueAtIndex(2, newModelValue);
        getView().setButton3Text(newModelValue);
    }

}