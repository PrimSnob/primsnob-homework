package com.primsnob.homework;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements MainView {
    private Button btnCounter1;
    private Button btnCounter2;
    private Button btnCounter3;

    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCounter1 = findViewById(R.id.btnCounter1);
        btnCounter2 = findViewById(R.id.btnCounter2);
        btnCounter3 = findViewById(R.id.btnCounter3);

        btnCounter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.button1Click();
            }
        });
        btnCounter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.button2Click();
            }
        });
        btnCounter3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.button3Click();
            }
        });

        presenter = new Presenter();
        presenter.bindView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    @Override
    public void setButton1Text(int value) {
        btnCounter1.setText("Количество = " + value);
    }

    @Override
    public void setButton2Text(int value) {
        btnCounter2.setText("Количество = " + value);
    }

    @Override
    public void setButton3Text(int value) {
        btnCounter3.setText("Количество = " + value);
    }
}