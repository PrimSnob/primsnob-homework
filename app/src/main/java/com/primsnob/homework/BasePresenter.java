package com.primsnob.homework;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<Model, View> {
    protected WeakReference<View> view;
    Model model;

    void setModel(Model model) {
        this.model = model;
    }

    public void bindView(@NonNull View view) {
        this.view = new WeakReference<>(view);
    }

    public void unbindView() {
        this.view = null;
    }

    protected View getView() {
        return view.get();
    }

}